#!/usr/bin/python3

import sys

if(len(sys.argv) != 2):
  print("Usage: temp.py file")
  sys.exit()

def minTemp():
  lines = [line.rstrip('\n') for line in open(sys.argv[1])]
  temps = [float(line.split("    ")[1]) for line in lines]
  dates = [line.split("    ")[0] for line in lines]
  print(dates[temps.index(min(temps))],"->", min(temps))
    
if __name__ == "__main__":
  minTemp()
