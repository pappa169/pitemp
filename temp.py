#!/usr/bin/python3

from lcd_i2c import lcd_init,lcd_release,lcd_print
import time

#kill signals
import signal
import os


class TempKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    lcd_print("Shutting down... ",1)
    lcd_print("",2)  
    self.kill_now = True
    
temp_sensor = '/sys/bus/w1/devices/28-80000028706f/w1_slave' 
temp_file = '/home/pi/git/pitemp/VAL'

def temp_sensor_ok():
  return os.path.isfile(temp_sensor)
    
def temp_raw():
    f = open(temp_sensor, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    file_ok,lines = temp_raw()
    while lines[0].strip()[-3:] != 'YES':
      time.sleep(0.2)
      lines = temp_raw()

    temp_output = lines[1].find('t=')
    if temp_output != -1:
      temp_string = lines[1].strip()[temp_output+2:]
      temp = float(temp_string) / 1000.0
      return temp 

def read_file_min_temp():
  lines = [line.rstrip('\n') for line in open(temp_file)]
  temps = [float(line.split("    ")[1]) for line in lines]
  dates = [line.split("    ")[0] for line in lines]
  return min(temps),dates[temps.index(min(temps))]
#  print(dates[temps.index(min(temps))],"->", min(temps))
  
    
if __name__ == "__main__":

  try:
    lcd_init()
    lcd_print("Starting... ",1)
    lcd_print("",2)
    min_temp,min_temp_date = read_file_min_temp()
    killer = TempKiller()
    while not killer.kill_now:
      if not temp_sensor_ok():
        lcd_print('Sensor error', 1)
      else:
        temp = read_temp()
        if read_temp() < min_temp :
          min_temp = temp
        lcd_print("Temp: "+str(temp),1)
        lcd_print("Min: " + str(min_temp),2)
      time.sleep(10)
      pass
  except KeyboardInterrupt:
    pass
  finally:
    lcd_release()